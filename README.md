# README #

This is for X4 monitoring boxes based upon the installs on Action Indoor Sports site.
They differ to the X2, X3 ones due to its use of direct sensors, an expansion module, and possibly PLC.

### What is this repository for? ###

* Monitoring performance of a single CW-X4 via direct sensors and Modbus TCP connection to the CW-X PLC.

### Who do I talk to? ###

* Jack Arney (Developer)
* Denny Hagias (Project Manager)
